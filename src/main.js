const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

/**
 * This arrow function return the result of raising the first operand to the power of the second operand.
 * @param {Number} a first param
 * @param {Number} b second param
 * @returns {Number}
 */
const exponentiation = (a, b) => {
  return a ** b
}

// Endpoint: http://localhost:${port}/pow?a=value&b=value
app.get('/pow', (req, res) =>{
  const {a, b} = req.query
  const pow = exponentiation(a, b)
  res.send(pow.toString())
})

app.listen(port, () => {
  console.log(`Server listening in http://localhost:${port}`)
})