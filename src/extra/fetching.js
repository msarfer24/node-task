const { default: axios } = require("axios")

function powURL(a, b) {
  return `http://localhost:3000/pow?a=${a}&b=${b}`
}

fetch(powURL(2,3))
  .then(res => res.json())
  .then(data => console.log(`fetch: ${data}`))


axios
  .get(powURL(2,3))
  .then(res => res.data)
  .then(data => console.log(`axios: ${data}`))
