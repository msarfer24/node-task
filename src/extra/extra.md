# How fetching data in javascript?

## Index
  * [Introduction](#introduction)
  * [Fetching data](#fetching-data)
    * [Fetch API](#fetch-api)
    * [Axios](#axios)

## Introduction
In javascript there are different ways to fetch data. But, first of all, what is fetching data.

## Fetching data
Is known as consuming an API from a server to get data. In the case of this writing I use my own API. This API has an endpoint, which return the result of raising the first operand to the power of the second operand.

```mermaid
stateDiagram-v2
    CLIENT --> API
    API --> CLIENT
    API --> SERVER
    SERVER --> API
```

![Fetching data|20x20](../assets/api.png)

### Fetch API

The first solution is experimental, but it is native to the platform, this is good because you know that it will be compatible with almost all browsers.

```js
function powURL(a, b) {
  return `http://localhost:3000/pow?a=${a}&b=${b}`
}

fetch(powURL(2,3))
  .then(res => res.json())
  .then(data => console.log(`fetch: ${data}`))
```

### Axios

This solution will require you to install an external package to handle the API request.

> :warning: **Warning:** Remember to install axios in package json.

```js
const { default: axios } = require("axios")

function powURL(a, b) {
  return `http://localhost:3000/pow?a=${a}&b=${b}`
}

axios
  .get(powURL(2,3))
  .then(res => res.data)
  .then(data => console.log(`axios: ${data}`))
```
