# Hello World API
In this repository I have develop a API (Application Programming Interface). It has only two functions, for now. You can see it in the next list:
  * [Greet](#greet)
  * [Exponentiate two numbers](#exponentiation)

## Greet
`http://localhost:3000`
```js
app.get('/', (req, res) => {
  res.send('Hello World!')
})
```
### Testing
```http
@baseurl = http://localhost:3000

### Send request to the server
GET {{baseurl}}/ HTTP/1.1
```

### Response
```js
Hello World!
```

## Exponentiation
`http://localhost:3000/pow?a=2&b=3`
```js
/**
 * This arrow function return the result of raising the first operand to the power of the second operand.
 * @param {Number} a first param
 * @param {Number} b second param
 * @returns {Number}
 */
const exponentiation = (a, b) => {
  return a ** b
}

app.get('/pow', (req, res) =>{
  const {a, b} = req.query
  const pow = exponentiation(a, b)
  res.send(pow.toString())
})
```

### Testing
```js
@baseurl = http://localhost:3000

### Send values a: 2 and b: 3
GET {{baseurl}}/pow?a=2&b=3
```

### Response
```js
8
```